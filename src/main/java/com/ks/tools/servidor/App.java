package com.ks.tools.servidor;

import com.ks.lib.Configuracion;

public class App
{
    public static void main(String[] args)
    {
        if (args.length >= 2)
        {
            ServidorTCP server = new ServidorTCP();
            server.setPuerto(Integer.parseInt(args[0]));
            if (Configuracion.getRuta().contains("\\"))
            {
                server.setFile(Configuracion.getRuta() + "transaction\\" + args[1]);
            }
            else
            {
                server.setFile(Configuracion.getRuta() + "transaction/" + args[1]);
            }
            server.openFile();
            server.conectar();

            try
            {
                Thread.currentThread().sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
